
.. cssclass:: table-bordered table-striped table-hover
.. list-table:: SIS8300 time system record list
  :widths: 25 50 10 5 10
  :header-rows: 1

  * - Record name
    - Description
    - Record type
    - Access
    - Asyn driver info
  * - $(P)$(R)EvrTimestampIn
    - Timestamp value obtained from EVR PV in *sec.nsec* format. EVR PV is specified in *TSEL* filed of this record. The value of this record is used by the asyn supported record *$(P)$(R)EvrTimestamp*.
    - stringin
    -
    -
  * - $(P)$(R)EvrTimestamp
    - Timestamp value for the driver. Used to set the timestamp of the records through the *evrTimeStampSource()* upon *updateTimeStamp()*. The same timestamp value can also be written to firmware register for DOD timestamping.
    - stringout
    - write
    - SIS8300.EVR.TIMESTAMP
  * - $(P)$(R)BeamModeEvr
    - Beam mode value obtained from EVR PV. EVR PV is specified in the *DOL* field of this record.
    - longout
    -
    -
  * - $(P)$(R)BeamModeUser
    - Beam mode value obtained from user (when EVR is not available).
    - longout
    -
    -
  * - $(P)$(R)BeamModeSource, $(P)$(R)BeamModeSourceR
    - Beam mode source selection:

      - User : use value from *$(P)$(R)BeamModeUser*
      - EVR : use value from *$(P)$(R)BeamModeEvr*

    - bo, bi
    - write, read
    - SIS8300.BEAM_MODE.SOURCE
  * - $(P)$(R)BeamModeSwitch
    - Beam mode value from the selected source through *$(P)$(R)BeamModeSource*. The value of this record is used by the asyn supported record *$(P)$(R)BeamModeVal*.
    - sel
    -
    -
  * - $(P)$(R)BeamModeVal, $(P)$(R)BeamModeValR
    - Beam mode for the driver. Value comes from *$(P)$(R)BeamModeSwitch* record.
    - longout, longin
    - write, read
    - SIS8300.BEAM_MODE.VALUE
  * - $(P)$(R)BeamDestEvr
    - Beam destination value obtained from EVR PV. EVR PV is specified in the *DOL* field of this record.
    - longout
    -
    -
  * - $(P)$(R)BeamDestUser
    - Beam destination value obtained from user (when EVR is not available).
    - longout
    -
    -
  * - $(P)$(R)BeamDestSource
    - Beam destination source selection:

      - User : use value from *$(P)$(R)BeamDestUser*
      - EVR : use value from *$(P)$(R)BeamDestEvr*

    - bo, bi
    - write, read
    - SIS8300.BEAM_DESTINATION.SOURCE
  * - $(P)$(R)BeamDestSwitch
    - Beam destination value from the selected source through *$(P)$(R)BeamDestSource*. The value of this record is used by the asyn supported record *$(P)$(R)BeamDestVal*.
    - sel
    -
    -
  * - $(P)$(R)BeamDestVal, $(P)$(R)BeamDestValR
    - Beam destination for the driver. Value comes from *$(P)$(R)BeamDestSwitch* record.
    - longout, longin
    - write, read
    - SIS8300.BEAM_DESTINATION.VALUE
  * - $(P)$(R)EvrBeamStateIn
    - Beam state value obtained from EVR PV. EVR PV is specified in the *INP* field of this record. The value of this record is used by the asyn supported record *$(P)$(R)EvrBeamState*.
    - longin
    -
    -
  * - $(P)$(R)EvrBeamState
    - Beam state for the driver. Value comes from *$(P)$(R)EvrBeamStateIn* record.
    - longout
    - write
    - SIS8300.EVR.BEAM_STATE
  * - $(P)$(R)EvrLinkIn
    - EVR link value obtained from EVR PV. EVR PV is specified in the *INP* field of this record. The value of this record is used by the asyn supported record *$(P)$(R)EvrLink*.
    - longin
    -
    -
  * - $(P)$(R)EvrLink
    - EVR link state for the driver. Value comes from *$(P)$(R)EvrLinkIn* record.
    - longout
    - write
    - SIS8300.EVR.LINK
  * - $(P)$(R)EvrLinkStatIn
    - EVR link alarm status obtained from EVR PV. EVR PV is specified in the *INP* field of this record. The value of this record is used by the asyn supported record *$(P)$(R)EvrLinkStat*.
    - longin
    -
    -
  * - $(P)$(R)EvrLinkStat
    - EVR link alarm status for the driver. Value comes from *$(P)$(R)EvrLinkStatIn* record.
    - longout
    - write
    - SIS8300.EVR.LINK_STATUS
  * - $(P)$(R)EvrLinkSevrIn
    - EVR link alarm severity obtained from EVR PV. EVR PV is specified in the *INP* field of this record. The value of this record is used by the asyn supported record *$(P)$(R)EvrLinkSevr*.
    - longin
    - None
    - None
  * - $(P)$(R)EvrLinkSevr
    - EVR link alarm severity for the driver. Value comes from *$(P)$(R)EvrLinkSevrIn* record.
    - longout
    - write
    - SIS8300.EVR.LINK_SEVERITY
