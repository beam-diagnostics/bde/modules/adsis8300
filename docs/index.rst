ADSIS8300
=========

Overview
--------

This is an `areaDetector <https://areadetector.github.io/master/index.html>`__ driver
for SIS8300 family of digitizers from `Struck Innovative Systeme <http://www.struck.de>`__.
It has been tested on the SIS8300-L(2) and SIS8300-KU AMCs. ADSIS8300 driver inherits
from `asynNDArrayDriver <https://areadetector.github.io/master/ADCore/NDArray.html#asynndarraydriver>`__. 
The driver exposes digitizers controling and monitoring features as a collection of EPICS records. 
Its primary use is with the generic firmware image from the vendor.
The driver can also be inherited by the higher level areaDetector drivers that might work on
specific firmware images such as for BPM or BCM systems.

Prerequisites
-------------

Linux kernel driver `sis8300drv`.

Functionality
-------------

The ADSIS8300 driver controls digitizer features such as clock source and frequency, trigger source, data acquisition, data transfer, and many more.

EPICS records
-------------

Apart from the the EPICS records inherited from the ``NDArrayBase.template`` file,
the ADSIS8300 driver defines additional EPICS records, specific to the SIS8300 board
and its data channels.

Board wide EPICS records
~~~~~~~~~~~~~~~~~~~~~~~~

These records are defined in the ``sis8300.template`` file. They contain board
specific records.

.. include:: sis8300.rst


Data channel EPICS records
~~~~~~~~~~~~~~~~~~~~~~~~~~

These records are defined in the ``sis8300-channel.template`` file. They contain per channel
specific records.

.. include:: sis8300-channel.rst


EVR related EPICS records
~~~~~~~~~~~~~~~~~~~~~~~~~

These records are defined in the ``sis8300-evr.template`` file. They contain records
that link to EVR IOC records (i.e. timestamp, beam mode & destination).

.. note:: The ``sis8300-evr.template`` records are not mandatory for SIS8300 operation and do not have to be loaded if EVR is not present. If they are loaded and EVR is not present, these records shall not interfere with the SIS8300 normal operation.

.. include:: sis8300-evr.rst
