
.. cssclass:: table-bordered table-striped table-hover
.. list-table:: SIS8300 channel record list
  :widths: 25 50 10 5 10
  :header-rows: 1

  * - Record name
    - Description
    - Record type
    - Access
    - Asyn driver info
  * - $(P)$(R)Name
    - Channel name.
    - stringout
    -
    -
  * - $(P)$(R)Control, $(P)$(R)ControlR
    - Channel enable / disable control. If channel is not enabled sample data will not be transferred from AMC memory to PV buffer.
    - bo, bi
    - write, read
    - SIS8300.CH.CONTROL
  * - $(P)$(R)Attenuation, $(P)$(R)AttenuationR
    - Attenuator value for I2C chip found on DWC8300-LF RTM.
    - ao, ai
    - write, read
    - SIS8300.CH.ATTENUATION
  * - $(P)$(R)InternalTriggerLength, $(P)$(R)InternalTriggerLengthR
    - 
    - longout, longin
    - write, read
    - SIS8300.CH.INTERNAL_TRIGGER_LENGTH
  * - $(P)$(R)InternalTriggerCondition, $(P)$(R)InternalTriggerConditionR
    - 
    - bo, bi
    - write, read
    - SIS8300.CH.INTERNAL_TRIGGER_CONDITION
  * - $(P)$(R)InternalTriggerOff, $(P)$(R)InternalTriggerOffR
    - 
    - longout, longin
    - write, read
    - SIS8300.CH.INTERNAL_TRIGGER_OFF
  * - $(P)$(R)InternalTriggerOn, $(P)$(R)InternalTriggerOnR
    - 
    - longout, longin
    - write, read
    - SIS8300.CH.INTERNAL_TRIGGER_ON
