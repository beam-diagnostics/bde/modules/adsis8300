#=================================================================#
# Template file: sis8300.template
# Database for the records specific to the Struck SIS8300 driver
# Hinko Kocevar
# June 12, 2018

include "NDArrayBase.template"

record(waveform, "$(P)$(R)DevicePathR")
{
    field(DTYP, "asynOctetRead")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.DEVICE_PATH")
    field(FTVL, "CHAR")
    field(NELM, "256")
    field(SCAN, "I/O Intr")
}

record(longin, "$(P)$(R)MemorySizeR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.MEMORY_SIZE")
    field(EGU,  "MB")
    field(SCAN, "I/O Intr")
}

record(waveform, "$(P)$(R)MessageR")
{
    field(DTYP, "asynOctetRead")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.DRIVER_MESSAGE")
    field(FTVL, "CHAR")
    field(NELM, "256")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)NumSamples")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.NUM_SAMPLES")
    field(DRVH, "$(MAX_SAMPLES=64)")
    field(DRVL, "64")
    field(PINI, "YES")
    field(VAL,  "$(MAX_SAMPLES=64)")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)NumSamplesR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.NUM_SAMPLES")
    field(SCAN, "I/O Intr")
}

record(mbbo, "$(P)$(R)ClockSource")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.CLOCK_SOURCE")
    field(ZRVL, "0")
    field(ZRST, "Internal")
    field(ONVL, "1")
    field(ONST, "RTM2")
    field(TWVL, "2")
    field(TWST, "SMA")
    field(THVL, "3")
    field(THST, "Harlink")
    field(FRVL, "4")
    field(FRST, "BackplaneA")
    field(FVVL, "5")
    field(FVST, "BackplaneB")
    field(SXVL, "6")
    field(SXST, "RTM01")
    field(PINI, "YES")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)ClockSourceR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.CLOCK_SOURCE")
    field(SCAN, "I/O Intr")
    field(ZRVL, "0")
    field(ZRST, "Internal")
    field(ONVL, "1")
    field(ONST, "RTM2")
    field(TWVL, "2")
    field(TWST, "SMA")
    field(THVL, "3")
    field(THST, "Harlink")
    field(FRVL, "4")
    field(FRST, "BackplaneA")
    field(FVVL, "5")
    field(FVST, "BackplaneB")
    field(SXVL, "6")
    field(SXST, "RTM01")
}

record(longout, "$(P)$(R)ClockDivider")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.CLOCK_DIVIDER")
    field(DRVL, "1")
    field(DRVH, "32")
    field(PINI, "YES")
    field(VAL,  "2")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)ClockDividerR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.CLOCK_DIVIDER")
    field(SCAN, "I/O Intr")
}

record(mbbo, "$(P)$(R)TriggerSource")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRIGGER_SOURCE")
    field(ZRVL, "0")
    field(ZRST, "Software")
    field(ONVL, "1")
    field(ONST, "External")
    field(TWVL, "2")
    field(TWST, "Internal")
    field(PINI, "YES")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)TriggerSourceR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRIGGER_SOURCE")
    field(SCAN, "I/O Intr")
    field(ZRVL, "0")
    field(ZRST, "Software")
    field(ONVL, "1")
    field(ONST, "External")
    field(TWVL, "2")
    field(TWST, "Internal")
}

record(mbbo, "$(P)$(R)TriggerExternalLine")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRIGGER_EXTERNAL_LINE")
    field(ZRVL, "0")
    field(ZRST, "FrontPanel0")
    field(ONVL, "1")
    field(ONST, "FrontPanel1")
    field(TWVL, "2")
    field(TWST, "FrontPanel2")
    field(THVL, "3")
    field(THST, "FrontPanel3")
    field(FRVL, "4")
    field(FRST, "BackPlane0")
    field(FVVL, "5")
    field(FVST, "BackPlane1")
    field(SXVL, "6")
    field(SXST, "BackPlane2")
    field(SVVL, "7")
    field(SVST, "BackPlane3")
    field(EIVL, "8")
    field(EIST, "BackPlane4")
    field(NIVL, "9")
    field(NIST, "BackPlane5")
    field(TEVL, "10")
    field(TEST, "BackPlane6")
    field(ELVL, "11")
    field(ELST, "BackPlane7")
    field(PINI, "YES")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)TriggerExternalLineR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRIGGER_EXTERNAL_LINE")
    field(SCAN, "I/O Intr")
    field(ZRVL, "0")
    field(ZRST, "FrontPanel0")
    field(ONVL, "1")
    field(ONST, "FrontPanel1")
    field(TWVL, "2")
    field(TWST, "FrontPanel2")
    field(THVL, "3")
    field(THST, "FrontPanel3")
    field(FRVL, "4")
    field(FRST, "BackPlane0")
    field(FVVL, "5")
    field(FVST, "BackPlane1")
    field(SXVL, "6")
    field(SXST, "BackPlane2")
    field(SVVL, "7")
    field(SVST, "BackPlane3")
    field(EIVL, "8")
    field(EIST, "BackPlane4")
    field(NIVL, "9")
    field(NIST, "BackPlane5")
    field(TEVL, "10")
    field(TEST, "BackPlane6")
    field(ELVL, "11")
    field(ELST, "BackPlane7")
}

record(longout, "$(P)$(R)TriggerDelay")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRIGGER_DELAY")
    field(DRVH, "0")
    field(DRVL, "-2046")
    field(PINI, "YES")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TriggerDelayR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRIGGER_DELAY")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)TriggerRepeat")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRIGGER_REPEAT")
    field(DRVH, "1000000")
    field(DRVL, "-1")
    field(PINI, "YES")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TriggerRepeatR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRIGGER_REPEAT")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)IrqPoll")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.IRQ_POLL")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(PINI, "YES")
    # default to IRQ polling
    field(VAL,  "1")
    info(autosaveFields, "VAL")
}

record(bi, "$(P)$(R)IrqPollR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.IRQ_POLL")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)Reset")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.RESET")
    field(ZNAM, "Normal")
    field(ONAM, "Reset")
}

record(mbbo, "$(P)$(R)RtmType")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.RTM_TYPE")
    field(ZRVL, "0")
    field(ZRST, "None")
    field(ONVL, "1")
    field(ONST, "SIS8900")
    field(TWVL, "2")
    field(TWST, "DWC8VM1")
    field(THVL, "3")
    field(THST, "DS8VM1")
    field(FRVL, "4")
    field(FRST, "DWC8300-LF")
    field(PINI, "YES")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)RtmTypeR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.RTM_TYPE")
    field(SCAN, "I/O Intr")
    field(ZRVL, "0")
    field(ZRST, "None")
    field(ONVL, "1")
    field(ONST, "SIS8900")
    field(TWVL, "2")
    field(TWST, "DWC8VM1")
    field(THVL, "3")
    field(THST, "DS8VM1")
    field(FRVL, "4")
    field(FRST, "DWC8300-LF")
}

record(ao, "$(P)$(R)SamplingFrequency")
{
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.SAMPLING_FREQUENCY")
    field(PREC, "1")
    field(EGU,  "Hz")
    field(PINI, "YES")
    field(VAL,  "125000000.0")
    info(autosaveFields, "VAL")
}

record(ai, "$(P)$(R)SamplingFrequencyR")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.SAMPLING_FREQUENCY")
    field(PREC, "1")
    field(EGU,  "Hz")
    field(SCAN, "I/O Intr")
}

record(ai, "$(P)$(R)TickValueR")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TICK_VALUE")
    field(PREC, "3")
    field(EGU,  "s")
    field(SCAN, "I/O Intr")
}

record(mbbi, "$(P)$(R)PcieLinkSpeedR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.PCIE_LINK_SPEED")
    field(SCAN, "I/O Intr")
    field(ZRVL, "0")
    field(ZRST, "Unknown")
    field(ONVL, "1")
    field(ONST, "Gen1")
    field(TWVL, "2")
    field(TWST, "Gen2")
    field(THVL, "4")
    field(THST, "Gen3")
}

record(mbbi, "$(P)$(R)PcieLinkWidthR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.PCIE_LINK_WIDTH")
    field(SCAN, "I/O Intr")
    field(ZRVL, "0")
    field(ZRST, "Unknown")
    field(ONVL, "1")
    field(ONST, "1 lane")
    field(TWVL, "2")
    field(TWST, "2 lanes")
    field(THVL, "4")
    field(THST, "4 lanes")
    field(FVVL, "8")
    field(FVST, "8 lanes")
}

record(ai, "$(P)$(R)PcieTransferTimeR") {
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.PCIE_TRANSFER_TIME")
    field(SCAN, "I/O Intr")
    field(PREC, "3")
    field(EGU,  "ms")
}

record(ai, "$(P)$(R)PcieStartTimeR") {
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.PCIE_START_TIME")
    field(SCAN, "I/O Intr")
    field(PREC, "3")
    field(EGU,  "ms")
}

record(ai, "$(P)$(R)PcieEndTimeR") {
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.PCIE_END_TIME")
    field(SCAN, "I/O Intr")
    field(PREC, "3")
    field(EGU,  "ms")
}

record(bi, "$(P)$(R)TransferArraysR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRANSFER_ARRAYS")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)TransferArrays")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRANSFER_ARRAYS")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(VAL,  "1")
    field(PINI, "YES")
    info(autosaveFields, "VAL")
}

record(bi, "$(P)$(R)TriggerTimeoutR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRIGGER_TIMEOUT")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)TriggerTimeoutValue")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRIGGER_TIMEOUT_VALUE")
    field(DRVL, "0")
    field(PINI, "YES")
    field(VAL,  "500")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TriggerTimeoutValueR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))SIS8300.TRIGGER_TIMEOUT_VALUE")
    field(SCAN, "I/O Intr")
}

###################################################################
#  The asynRecord is used for mainly for trace mask               #
###################################################################

# Set ASYN_TRACEIO_HEX bit by default
record(asyn, "$(P)$(R)AsynIO")
{
    field(PORT, $(PORT))
    field(TIB2, "1")
    info("ADType", "ADDriver")
}
